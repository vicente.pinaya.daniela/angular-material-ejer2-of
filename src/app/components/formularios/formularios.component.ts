import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-formularios',
  templateUrl: './formularios.component.html',
  styleUrls: ['./formularios.component.css']
})
export class FormulariosComponent implements OnInit {
   //lista form3:
  listaOficial:string[]=[];
  //lista form2
  lista:string[]=[];
  form!:FormGroup;
  v:boolean=true;
  constructor(
              private _snackbar:MatSnackBar,
              private fb:FormBuilder,
              ) {
    this.cargarForm1();
    this.cargarListaOficial();

   }

   get getListaform1(){
    return this.form.get('listatexto') as FormArray;
  }

  ngOnInit(): void {
  }


   //FOMR1
  

  cargarForm1(){
    this.form = this.fb.group({
      listatexto:this.fb.array([])
    });

    this.getListaform1.push(this.fb.group({
      texto: [null,[Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
      ValordelBoton : [null,Validators.required]
    }),
    );
    this.getListaform1.push(this.fb.group({
      texto: [null,[Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
      ValordelBoton : [null,Validators.required]
    }),
    );
    this.getListaform1.push(this.fb.group({
      texto: [null,[Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
      ValordelBoton : [null,Validators.required]
    }),
    );
    this.getListaform1.push(this.fb.group({
      texto: [null,[Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
      ValordelBoton : [null,Validators.required]
    }),
    );
    this.getListaform1.push(this.fb.group({
      texto: [null,[Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
      ValordelBoton : [null,Validators.required]
    }),
    );
    this.getListaform1.push(this.fb.group({
      texto: [null,[Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
      ValordelBoton : [null,Validators.required]
    }),
    );

     
    
  }
  
  ponerValor(event:any,id:number){
    if (this.getListaform1.at(id).get('texto')?.valid) {
      if(event.keyCode==13){
       
        this.getListaform1.at(id).get('ValordelBoton')?.setValue(this.getListaform1.at(id).get('texto')?.value);
      }
    }
  }

  guardarAForm2(){
    this.cargarLista() 
     
  }

   //FOMR2
  
  cargarLista(){
    
    for (let i = 0; i < this.getListaform1.length; i++) {
        if (this.getListaform1.at(i).get('ValordelBoton')?.valid) {
          
          this.lista.push(this.getListaform1.at(i).get('ValordelBoton')?.value);
        }
    }

  }


  eliminar(id:number){
    const opcion = confirm('Estas seguro de eliminar el texto');
       if (opcion) {
        this.lista =this.lista.filter((data,i) => {
          return i!=id;
        })
       this._snackbar.open('El usuario fue eliminado con exito', '', {
         duration: 1500,
         horizontalPosition: 'center',
         verticalPosition: 'bottom',
       });
     }
  }

  Adicionar(){
    this.cargarListaOficial()
  }

  Limpiar(){
    this.lista=[];
  }


  //FOMR3
  cargarListaOficial(){
    this.lista.forEach(e =>{
      this.listaOficial.push(e)
    });
  }

}